# coding=utf-8
import datetime
import errno
import json
import os
import pandas as pd
import requests
import time
from pandas.io.json import json_normalize

output_directory = str(datetime.datetime.today())[:10] + "_conus"
try:
    os.makedirs(output_directory)
except OSError as e:
    if e.errno != errno.EEXIST:
        raise

states = [
    "AL",
    'AZ',
    'AR',
    'CA',
    'CO',
    'CT',
    'DE',
    'FL',
    'GA',
    'ID',
    'IL',
    'IN',
    'IA',
    'KS',
    'KY',
    'LA',
    'ME',
    'MD',
    'MA',
    'MI',
    'MN',
    'MS',
    'MO',
    'MT',
    'NE',
    'NV',
    'NH',
    'NJ',
    'NM',
    'NY',
    'NC',
    'ND',
    'OH',
    'OK',
    'OR',
    'PA',
    'RI',
    'SC',
    'SD',
    'TN',
    'TX',
    'UT',
    'VT',
    'VA',
    'WA',
    'WV',
    'WI',
    'WY'
    # ,"HI", "AK"

    # #US Territories
    # 'AS',
    'DC',
    # 'FM',
    # 'GU',
    # 'MH',
    # 'MP',
    # 'PW',
    # 'PR',
    # 'VI',
]

# Start GlobalVariables

'''Precipitation Threshold: This is determined by the number of missing days of observation in any given calendar month.'''
precipThreshold = 2

'''Temperature Threshold: This is determined by the number of missing days of observation in any given calendar month.'''
tempThreshold = 5

responsibleQuery = False
verbose = True

count_stations = 0
count_reliable_temp = 0
count_reliable_precip = 0
count_states = 0

def main():
    global count_reliable_temp, count_reliable_precip, count_stations, count_states
    conus_station_list = []
    conus_black_temp_list = []
    conus_white_temp_list = []
    conus_black_precip_list = []
    conus_white_precip_list = []
    for state in states:
        state_black_temp_list = []
        state_white_temp_list = []
        state_black_precip_list = []
        state_white_precip_list = []
        if responsibleQuery:
            time.sleep(20)
        station_list = get_state_stations(state)
        for station in station_list:
            params = {'sid': station[2], 'sdate': 'por', 'edate': 'por', 'elems': "maxt,mint,pcpn"}
            r = requests.get('https://data.rcc-acis.org/StnData', params=params)
            try:
                data = json.loads(r.text)
                df = json_normalize(data, 'data')
            except:
                print("Failed to Parse JSON for station: {}".format(station[2]))
                continue
            df = df.set_index(pd.to_datetime(df[0]))
            del df[0]
            if assert_temp_reliable(station, df):
                state_white_temp_list.append(station)
                print("+T", end='')
            else:
                state_black_temp_list.append(station)
                print("-T", end='')
            if assert_precip_reliable(station, df):
                state_white_precip_list.append(station)
                print("+P", end='')
            else:
                state_black_precip_list.append(station)
                print("-P", end='')
            print('')

        conus_station_list += station_list
        conus_black_temp_list += state_black_temp_list
        conus_black_precip_list += state_black_precip_list
        conus_white_temp_list += state_white_temp_list
        conus_white_precip_list += state_white_precip_list
        print("{} Count Temperature Passed: {}".format(state, len(state_white_temp_list)))
        print("{} Count Precipitation Passed: {}".format(state, len(state_white_precip_list)))
        print("{} Count Total: {}".format(state, len(station_list)))
        count_reliable_temp += len(state_white_temp_list)
        count_reliable_precip += len(state_white_precip_list)
        count_stations += len(station_list)
        count_states += 1

        # save_state_output(state, station_list, state_white_temp_list, state_white_precip_list)
    save_json_output(conus_station_list, conus_white_temp_list, conus_white_precip_list)
    print("CONUS Count Temperature Passed: {}".format(count_reliable_temp))
    print("CONUS Count Precipitation Passed: {}".format(count_reliable_precip))
    print("CONUS Count Total: {}".format(count_stations))
    print("CONUS State Total: {}".format(count_states))


def mean(iterable):
    a = sum(iterable)
    if len(iterable) == 0:
        return a / 1
    else:
        return a / len(iterable)


def get_state_stations(state):
    if verbose:
        print("Populating Station List for: " + state)
    station_id_list = []
    payload = {'state': state}
    r = requests.get('https://data.rcc-acis.org/StnMeta', params=payload)
    data = r.json()
    for stations in data['meta']:
        if len(stations['sids']) != 0:
            if "ll" not in stations:
                continue
            for id in stations['sids']:
                if id.startswith('USW') or id.startswith('USC'):
                    station_id_list.append([stations['name'], stations['ll'], id[:11]])
    if verbose:
        print("Stations: {}".format(len(station_id_list)))
    return station_id_list


def assert_temp_reliable(station, df) -> bool:
    missing_reports = df.loc[(df[1] == 'M') | (df[2] == 'M')]
    missing_months = missing_reports.groupby(pd.Grouper(freq='M'), sort=False).size().reset_index(name='count')
    if missing_months['count'].max() > tempThreshold:
        # if verbose:
        #     print("Failed Station: {} based on exceeding Temp threshold value: {} with value: {}"
        #           .format(station,
        #                   tempThreshold,
        #                   str(missing_months['count'].max())))
        return False
    else:
        return True


def assert_precip_reliable(station, df):
    missing_reports = df.loc[df[3] == 'M']
    missing_months = missing_reports.groupby(pd.Grouper(freq='M'), sort=False).size().reset_index(name='count')
    if missing_months['count'].max() > precipThreshold:
        # if verbose:
        #     print("Failed Station: {} based on exceeding Precip threshold value: {} with value: {}"
        #           .format(station,
        #                   precipThreshold,
        #                   str(missing_months['count'].max())))
        return False
    else:
        return True


#
# def save_state_output(state, station_list, white_temp_list, white_precip_list):
#     global count_reliable_precip, count_reliable_temp, count_stations
#     with open("{}/{}_station_reliability.csv".format(output_directory, state), "w") as stateOutput:
#         stateOutput.write("Assessment run on: " + str(datetime.datetime.now()) + "\n")
#         if len(station_list) != 0:
#             stateOutput.write("Count Temperature Passed: {}".format(len(white_temp_list)))
#             stateOutput.write("Count Precipitation Passed: {}".format(len(white_precip_list)))
#             stateOutput.write("Count Total: {}".format(len(station_list)))
#
#
#         stateOutput.write("-----------------------------------------------------------------------------\n")
#         stateOutput.write("Station Name Lat/Long StationID Temp. Passed  Precip. Passed\n")
#
#         for station in station_list:
#             reliable_temp = station in white_temp_list
#             reliable_precip = station in white_precip_list
#             if reliable_temp or reliable_precip:
#                 stateOutput.write(str(station[0]) + " , " + str(station[1]) + ", " + str(station[2]) + ", " + str(
#                     int(reliable_temp)) + ", " + str(int(reliable_precip)) + "\n")


def save_json_output(station_list, white_temp_list, white_precip_list):
    output = []
    for station in station_list:
        reliable_temp = station in white_temp_list
        reliable_precip = station in white_precip_list
        # if reliable_temp or reliable_precip:
        output.append({'id': station[2],
                       'name': station[0],
                       'lon': station[1][0],
                       "lat": station[1][1],
                       'temp': int(reliable_temp),
                       "precip": int(reliable_precip)
                       })

    with open("{}/conus_station_reliability_all.json".format(output_directory), "w") as jsonOutput:
        json.dump(output, jsonOutput, sort_keys=True, indent=4, separators=(',', ':'))


if __name__ == '__main__':
    main()
